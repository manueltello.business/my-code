package workers

import (
	"bytes"
	"encoding/json"
	"errors"
	"io"
	"net/http"

	router "github.com/ManuelTello/my-code/source/router"
	assets "github.com/ManuelTello/my-code/tests/assets"
)

type GlobalSession struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type Token struct {
	Token string `json:"token"`
}

func CreateUser(router *router.Router) error {
	/*
		Test to create a new user
	*/
	bodyJSON, marshalError := json.Marshal(GlobalSession{Username: "test", Password: "12345"})
	if marshalError == nil {
		body_reader := bytes.NewReader(bodyJSON)
		responseRecorder, request := assets.CreateResponseAndRequest("POST", "/session/signup", body_reader)
		router.ServeHTTP(responseRecorder, request)
		result := responseRecorder.Result().StatusCode
		if result == http.StatusInternalServerError || result == http.StatusNotFound {
			result_error := errors.New("could not complete request bad/internal error")
			return result_error
		} else {
			return nil
		}
	} else {
		return marshalError
	}
}

func RetrieveJWTToken(router *router.Router, token *Token) error {
	/*
		Test to retrieve and save the token as a cookie
	*/
	bodyJSON, marshalError := json.Marshal(GlobalSession{Username: "test", Password: "12345"})
	if marshalError == nil {
		body_reader := bytes.NewReader(bodyJSON)
		responseRecorder, request := assets.CreateResponseAndRequest("POST", "/session/signin", body_reader)
		router.ServeHTTP(responseRecorder, request)
		result := responseRecorder.Result().StatusCode
		if result == http.StatusInternalServerError || result == http.StatusNotFound {
			result_error := errors.New("could not complete request bad/internal error")
			return result_error
		} else {
			if readBytes, readError := io.ReadAll(responseRecorder.Body); readError == nil {
				if unmarshallError := json.Unmarshal(readBytes, token); unmarshallError == nil {
					return nil
				} else {
					return unmarshallError
				}
			} else {
				return readError
			}
		}
	} else {
		return marshalError
	}
}

func JWTTokenHealthCheck(router *router.Router) error {
	return nil
}
