package account_test

import (
	"testing"

	workers "github.com/ManuelTello/my-code/tests/account/workers"
	assets "github.com/ManuelTello/my-code/tests/assets"
)

func TestMain(t *testing.T) {
	if mainApp, err := assets.CreateApp(); err == nil {
		router := mainApp.Router
		var token *workers.Token = nil

		t.Run("TestSignUp", func(t *testing.T) {
			workerError := workers.CreateUser(router)
			if workerError != nil {
				t.Fatal(workerError)
			}
		})

		t.Run("TestSignIn", func(t *testing.T) {
			workerError := workers.RetrieveJWTToken(router, token)
			if workerError != nil {
				t.Fatal(workerError)
			}
		})

		t.Run("TestTokenHealthCheck", func(t *testing.T) {
			workerError := workers.JWTTokenHealthCheck(router)
			if workerError != nil {
				t.Fatal(workerError)
			}
		})
	} else {
		t.Fatal("could not create main app.", err)
	}
}
