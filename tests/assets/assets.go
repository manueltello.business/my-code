package assets

import (
	"io"
	"net/http"
	"net/http/httptest"
	"os"

	app "github.com/ManuelTello/my-code/source/providers/app"
)

func CreateResponseAndRequest(request_method, request_target string, request_body io.Reader) (*httptest.ResponseRecorder, *http.Request) {
	response_recorder := httptest.NewRecorder()
	request := httptest.NewRequest(request_method, request_target, request_body)
	request.Header.Set("Authorization", "ApiKey "+os.Getenv("API_KEY"))
	request.Header.Set("Content-Type", "application/json")

	return response_recorder, request
}

func CreateApp() (*app.App, error) {
	new_app := app.NewApp()

	if envError := new_app.MountEnvironmentVariables(); envError != nil {
		return nil, envError
	} else {
		os.Setenv("GOLANG_ENVIRONMENT", "TEST")
	}

	if databaseError := new_app.CreateDatabaseContext(); databaseError != nil {
		return nil, databaseError
	}
	new_app.MountRouter()

	return new_app, nil
}
