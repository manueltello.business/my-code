package main

import (
	"log"
	"os"

	app "github.com/ManuelTello/my-code/source/providers/app"
)

func main() {
	mainApp := app.NewApp()

	if envError := mainApp.MountEnvironmentVariables(); envError != nil {
		log.Fatalln(envError)
		os.Exit(1)
	}

	if createContextError := mainApp.CreateDatabaseContext(); createContextError != nil {
		log.Fatalln(createContextError)
		os.Exit(1)
	}

	mainApp.MountRouter()

	if serverError := mainApp.StartServer(); serverError != nil {
		log.Fatalln(serverError)
		os.Exit(1)
	}
}
