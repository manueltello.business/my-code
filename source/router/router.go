package router

import (
	"net/http"
	"os"
	"regexp"
)

type MiddlewareFunction = func(rw http.ResponseWriter, r *http.Request)

type IRouter interface {
	ServeHTTP(rw http.ResponseWriter, r *http.Request)
	Post(path string, middleware MiddlewareFunction)
}

type Route struct {
	Path       string
	Method     string
	Middleware MiddlewareFunction
	Next       *Route
}

type Router struct {
	RouteHead *Route
}

func (router *Router) ServeHTTP(rw http.ResponseWriter, r *http.Request) {
	/*
		If a panic occurs this will catch it and send an internal server
		error response.
	*/
	defer func() {
		if serverError := recover(); serverError != nil {
			response_body := []byte("500 internal server error")
			rw.WriteHeader(http.StatusInternalServerError)
			rw.Write(response_body)
		}
	}()

	/*
		Checks if the api key is present in the header.
	*/
	regex_pattern := regexp.MustCompile(`^\w* {1}`)
	raw_request_header := r.Header.Get("Authorization")
	authorization_key_exctracted := regex_pattern.ReplaceAllString(raw_request_header, "")
	api_key := os.Getenv("API_KEY")

	if authorization_key_exctracted != api_key {
		response_body := []byte("401 not authorized.")
		rw.WriteHeader(http.StatusForbidden)
		rw.Write(response_body)
	} else {
		var route_match *Route = nil
		route_aux := router.RouteHead
		request_method := r.Method
		request_path := r.URL.Path

		for route_aux != nil {
			if route_aux.Method == request_method && route_aux.Path == request_path {
				route_match = route_aux
			}

			route_aux = route_aux.Next
		}

		if route_match == nil {
			response_body := []byte("404 not found.")
			rw.WriteHeader(http.StatusNotFound)
			rw.Write(response_body)
		} else {
			route_match.Middleware(rw, r)
		}
	}
}

func (router *Router) Post(path string, middleware MiddlewareFunction) {
	if router.RouteHead == nil {
		router.RouteHead = new(Route)
		router.RouteHead.Next = nil
		router.RouteHead.Path = path
		router.RouteHead.Method = "POST"
		router.RouteHead.Middleware = middleware
	} else {
		route_aux := router.RouteHead

		for route_aux.Next != nil {
			route_aux = route_aux.Next
		}

		route_aux.Next = new(Route)
		route_aux.Next.Next = nil
		route_aux.Next.Path = path
		route_aux.Next.Method = "POST"
		route_aux.Next.Middleware = middleware
	}
}

func NewRouter() *Router {
	router := new(Router)
	router.RouteHead = nil

	return router
}
