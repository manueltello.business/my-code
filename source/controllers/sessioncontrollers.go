package session_controller

import "net/http"

type ISessionController interface {
	ValidateSignUp(rw http.ResponseWriter, r *http.Request)
	ValidateSignIn(rw http.ResponseWriter, r *http.Request)
	TokenHealthCheck(rw http.ResponseWriter, r *http.Request)
}

type SessionController struct {
}

func (controller *SessionController) ValidateSignUp(rw http.ResponseWriter, r *http.Request) {

}

func (controller *SessionController) ValidateSignIn(rw http.ResponseWriter, r *http.Request) {

}

func (controller *SessionController) TokenHealthCheck(rw http.ResponseWriter, r *http.Request) {

}

func NewController() SessionController {
	var (
		new_controller SessionController
	)

	return new_controller
}
