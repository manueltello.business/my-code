package configuration

import (
	"os"
	"path/filepath"
	"regexp"
)

type IConfiguration interface {
}

type Configuration struct {
	AbsRootFolder string
}

func NewConfiguration() *Configuration {
	new_configuration := new(Configuration)
	relative_working_directory, _ := os.Getwd()

	for !regexp.MustCompile(`my-code$`).Match([]byte(relative_working_directory)) {
		relative_working_directory = filepath.Join(relative_working_directory, "..")
	}

	new_configuration.AbsRootFolder = relative_working_directory

	return new_configuration
}
