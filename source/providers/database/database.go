package database

import (
	"database/sql"

	"github.com/go-sql-driver/mysql"
	_ "github.com/mattn/go-sqlite3"
)

type IDatabase interface {
	CreateTestingConnection() error
	CreateDevelopmentConnection() error
	CreateProductionConnection() error
	GetConnection() *sql.DB
}

type Database struct {
	connection *sql.DB
}

func (database *Database) CreateTestingConnection(data_source string) error {
	openConnection, connectionError := sql.Open("sqlite3", data_source)
	if connectionError != nil {
		return connectionError
	} else {
		database.connection = openConnection
		return nil
	}
}

func (database *Database) CreateDevelopmentConnection(data_source string) error {
	openConnection, connectionError := sql.Open("sqlite3", data_source)
	if connectionError != nil {
		return connectionError
	} else {
		database.connection = openConnection
		return nil
	}
}

func (database *Database) CreateProductionConnection(database_user, database_password, database_host, database_schema string) error {
	connectionOptions := mysql.Config{
		User:      database_user,
		Passwd:    database_password,
		DBName:    database_schema,
		Addr:      database_host,
		ParseTime: true,
	}

	openConection, connectionError := sql.Open("mysql", connectionOptions.FormatDSN())
	if connectionError != nil {
		return connectionError
	} else {
		database.connection = openConection
		return nil
	}
}

func (database *Database) GetConnection() *sql.DB {
	return database.connection
}

func NewDatabaseContext() *Database {
	new_database_context := new(Database)
	return new_database_context
}
