package app

import (
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strings"

	configuration "github.com/ManuelTello/my-code/source/providers/configuration"
	database "github.com/ManuelTello/my-code/source/providers/database"
	router "github.com/ManuelTello/my-code/source/router"
)

type IApp interface {
	StartServer() error
	CreateDatabaseContext() error
	MountEnvironmentVariables() error
	MountRouter()
	GetDatabase() *database.Database
}

type App struct {
	Router        *router.Router
	database      *database.Database
	Configuration *configuration.Configuration
}

func (app *App) StartServer() error {
	var port string = os.Getenv("APP_PORT")
	log.Print("Server started.")
	if serverError := http.ListenAndServe(port, app.Router); serverError == nil {
		return nil
	} else {
		return serverError
	}
}

func (app *App) CreateDatabaseContext() error {
	var environment string = os.Getenv("GOLANG_ENVIRONMENT")
	app.database = database.NewDatabaseContext()
	var (
		err error
	)

	switch environment {
	case "DEVELOPMENT":
		dataSource := "file:development_database.db?cache=shared&mode=memory"
		err = app.database.CreateDevelopmentConnection(dataSource)
	case "TEST":
		path := filepath.Join(app.Configuration.AbsRootFolder, "tests", "test_database.db")
		dataSource := "file:" + path + "?cache=shared&mode=rwc"
		err = app.database.CreateTestingConnection(dataSource)
	case "PRODUCTION":
		err = app.database.CreateProductionConnection(os.Getenv("DATABASE_USER"), os.Getenv("DATABASE_PASSWORD"), os.Getenv("DATABASE_HOST"), os.Getenv("DATABASE_SCHEMA"))
	}

	return err
}

func (app *App) MountEnvironmentVariables() error {
	environment := os.Getenv("GOLANG_ENVIRONMENT")
	if environment != "PRODUCTION" {
		envFilePath := filepath.Join(app.Configuration.AbsRootFolder, ".env")
		if file, readError := os.ReadFile(envFilePath); readError == nil {
			lineList := strings.Split(string(file), "\r\n")
			for _, e := range lineList {
				key_value := strings.Split(e, "=")
				os.Setenv(key_value[0], key_value[1])
			}
			return nil
		} else {
			return readError
		}
	}
	return nil
}

func (app *App) MountRouter() {
	app.Router = new(router.Router)

	/*
		Controllers goes here
	*/
}

func (app *App) GetDatabase() *database.Database {
	return app.database
}

func NewApp() *App {
	new_app := new(App)
	new_app.Configuration = configuration.NewConfiguration()

	return new_app
}
